FROM centos:centos7

ADD src /opt/src

RUN yum install -y \
    /opt/src/jre-8u60-linux-x64.rpm \
    && yum clean all \
    && rm -rf /opt/src/*

ENTRYPOINT [ "java" ]

CMD [ "-version" ]

